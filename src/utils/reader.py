import numpy as np

def read_lhd(filename, n, d):
    X = []
    with open(filename, mode="r") as f:
        X = f.read().split(",")
    return np.asarray(X, dtype="float").reshape(n,d)


def normalize(x):
    return (x - x.min(0)) / x.ptp(0)


def load_lhd_from_file(filename, n, d):
    return normalize(read_lhd(filename, n, d))
