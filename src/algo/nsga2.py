# -*- coding: utf-8 -*-

import numpy as np
from deap import creator, base, tools, algorithms

import constants as const

from utils import load_lhd_from_file
from algo.mo import MultiObjectiveAlgorithm
from criterion import KullbackLeibler, AudzeEglais
from visualization import visualize_individuals, visualize_pareto, visualize_criterion, visualize_chosen_individuals, visualize_ind


class NSGAII(MultiObjectiveAlgorithm):

    def __init__(self, mu, lbda, cxpb, mutpb, ngen, n, d):
        MultiObjectiveAlgorithm.__init__(self, ngen, n, d)

        self.eval_func = {
            "Kullback Leibler": self.kl.H,
            "Projection Kullback Leibler": self.kl.H_projection,
            "Gini": self.gini.indice
        }

        self.mu = mu
        self.lbda = lbda
        self.cxpb = cxpb
        self.mutpb = mutpb

        self.hof = None
        self.stats = None

        creator.create("FitnessMulti", base.Fitness, weights=(1.0, 1.0, -1.0))
        creator.create("Individual", np.ndarray, fitness=creator.FitnessMulti)

        self.toolbox = base.Toolbox()

        self.toolbox.register(
            "individual", self.init_float_individual, creator.Individual)
        self.toolbox.register("population", tools.initRepeat,
                              list, self.toolbox.individual)

        self.toolbox.register("evaluate", self.evaluation,
                              func_list=self.eval_func.values())
        self.toolbox.register("mate", tools.cxOnePoint)

        # individual – Sequence individual to be mutated.
        # eta – Crowding degree of the mutation. A high eta will produce a mutant resembling its parent, while a small eta will produce a solution much more different.
        # low – A value or a sequence of values that is the lower bound of the search space.
        # up – A value or a sequence of values that is the upper bound of the search space.

        self.toolbox.register(
            "mutate", tools.mutPolynomialBounded, eta=1, low=0, up=1, indpb=1
        )
        self.toolbox.register("select", tools.selNSGA2)

    def run(self):
        pop = self.toolbox.population(n=self.mu)
        hof = tools.ParetoFront(similar=np.array_equal)
        stats = tools.Statistics(lambda ind: ind.fitness.values)
        stats.register("avg", np.mean, axis=0)
        stats.register("std", np.std, axis=0)
        stats.register("min", np.min, axis=0)
        stats.register("max", np.max, axis=0)

        #     population – A list of individuals.
        #     toolbox – A Toolbox that contains the evolution operators.
        #     mu – The number of individuals to select for the next generation.
        #     lambda_ – The number of children to produce at each generation.
        #     cxpb – The probability that an offspring is produced by crossover.
        #     mutpb – The probability that an offspring is produced by mutation.
        #     ngen – The number of generation.
        #     stats – A Statistics object that is updated inplace, optional.
        #     halloffame – A HallOfFame object that will contain the best individuals, optional.

        pop, stats = algorithms.eaMuPlusLambda(
            pop, self.toolbox, self.mu, self.lbda, self.cxpb, self.mutpb, self.ngen, stats, halloffame=hof
        )

        self.stats = stats
        self.hof = hof

    def plot(self):
        for i, key in enumerate(self.eval_func):
            if key == "Gini":
                criterion_over_gen = [item["min"][i] for item in self.stats]
            else:
                criterion_over_gen = [item["max"][i] for item in self.stats]

            visualize_criterion(key, self.ngen, criterion_over_gen)

        lhd, lhd_score = self.add_lhd_to_pareto()
        if len(lhd_score) == 2:
            visualize_pareto(self.hof, (0,1), lhd_score, list(self.eval_func.keys()))
        elif len(lhd_score) == 3:
            eval_funct = list(self.eval_func.keys())
            visualize_pareto(self.hof, (0,1), lhd_score, eval_funct)
            visualize_pareto(self.hof, (0,2), lhd_score, eval_funct)
            visualize_pareto(self.hof, (1,2), lhd_score, eval_funct)

        if lhd is not None:
            print("LHD score")
            print(lhd_score)
            visualize_ind(lhd, "LHD", self.kl)
        visualize_chosen_individuals(
            self.hof, self.n_samples, self.n_variables, self.kl)

    def compute_criteria_lhd(self, lhd):
        return self.evaluation(lhd, func_list=self.eval_func.values())

    def add_lhd_to_pareto(self):
        values = None
        lhd = None
        try:
            dirname = "./resources/"
            filename = str(self.n_samples) + "_" + \
                str(self.n_variables) + "_" + "lhd.txt"
            lhd = load_lhd_from_file(
                dirname + filename, self.n_samples, self.n_variables)
            values = self.compute_criteria_lhd(lhd)

        except FileNotFoundError:
            print("No file found to load LHD for (" +
                  str(self.n_samples) + "," + str(self.n_variables) + ")")

        return lhd, values
