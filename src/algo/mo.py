import numpy
from criterion import KullbackLeibler, Gini


class MultiObjectiveAlgorithm:

    __slots__ = ['ngen', 'n_samples', 'n_variables', 'kl', 'gini']

    def __init__(self, ngen, n_samples, n_variables):
        self.ngen = ngen
        self.n_samples = n_samples
        self.n_variables = n_variables
        self.kl = KullbackLeibler(self.n_samples, self.n_variables, cache=True)
        self.gini = Gini(self.n_samples, self.n_variables)

    def criteria(self, func_list, X, **kwargs):
        return [f(X, **kwargs) for f in func_list]

    def generate_random_float_array(self):
        return numpy.random.rand(self.n_samples, self.n_variables).flatten()

    def generate_random_int_array(self):
        return numpy.random.randint(
            low=0, high=self.n_samples, size=(self.n_samples, self.n_variables)
        ).flatten()

    def evaluation(self, individual, func_list):
        return self.criteria(func_list, individual, d=self.n_variables)

    def init_int_individual(self, icls):
        return icls(self.generate_random_int_array())

    def init_float_individual(self, icls):
        return icls(self.generate_random_float_array())
