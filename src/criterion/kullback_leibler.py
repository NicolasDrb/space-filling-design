import numpy as np
import math

from .criterion import Criterion


class KullbackLeibler(Criterion):

    def __init__(self, n_samples, n_variables, cache=False):
        Criterion.__init__(self, n_samples, n_variables)

    def H(self, X, d, **kwargs):
        n = self.n_samples
        X = X.reshape(n, d)
        H = 0

        for k in range(n):
            H += np.log(self.f(X, k, d))

        H = -(1 / n) * H
        return H

    def f(self, X, k, d):
        n = self.n_samples
        h = (1 / math.sqrt(12)) * (1 / n ** (1 / (d + 4)))
        f = 0

        for i in range(n):
            f += self.K((X[k] - X[i]) / h, d)

        f = (1 / (n * (h ** d))) * f
        return f

    def K(self, z, d):
        if len(z) == 1:
            norm = abs(z[0])
        else:
            norm = np.linalg.norm(z)

        s = d / 12
        C = ((2 * math.pi) ** (-d / 2)) / (s ** d)
        K = C * np.exp(-(1 / (2 * s ** 2)) * norm ** 2)
        return K

    def H_projection(self, X, **kwargs):
        res = 0
        X = X.reshape(self.n_samples, self.n_variables)
        for axe in range(self.n_variables):
            x = X[:, axe]
            res += self.H(x, 1)
        return res
