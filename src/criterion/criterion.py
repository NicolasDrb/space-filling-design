from visualization import visualize_criterion


class Criterion:

    __slots__ = ['n_samples', 'n_variables']

    def __init__(self, n_samples, n_variables):
        self.n_samples = n_samples
        self.n_variables = n_variables

    @staticmethod
    def plot(name, ngen, hof):
        visualize_criterion(name, ngen, hof)
