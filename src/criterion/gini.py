import numpy as np

from .criterion import Criterion
from .kullback_leibler import KullbackLeibler


class Gini(Criterion):

    def __init__(self, n_samples, n_variables):
        Criterion.__init__(self, n_samples, n_variables)
        self.kl = KullbackLeibler(n_samples, n_variables)

    def indice(self, X, **kwargs):
        repartitions = np.absolute(self.get_repartitions(X))

        return self.E(repartitions) / (2 * np.mean(repartitions))

    def E(self, values):
        return sum([abs(values[i] - values[j]) for i in range(self.n_variables) for j in range(self.n_variables)]) / self.n_variables ** 2

    def get_repartitions(self, X):
        res = np.empty(shape=self.n_variables)
        X = X.reshape(self.n_samples, self.n_variables)

        for axe in range(self.n_variables):
            x = X[:, axe]
            res[axe] = self.kl.H(x, 1)

        return res
