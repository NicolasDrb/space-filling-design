from .kullback_leibler import KullbackLeibler
from .audze_eglais import AudzeEglais
from .gini import Gini
