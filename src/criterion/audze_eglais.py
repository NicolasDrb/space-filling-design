from .criterion import Criterion


class AudzeEglais(Criterion):

    def __init__(self, n_samples, n_variables):
        Criterion.__init__(self, n_samples, n_variables)

    def ae_objective(self, X):
        X = X.reshape(self.n_samples, self.n_variables)
        res = 0
        for i in range(self.n_samples - 1):
            for j in range(i + 1, self.n_samples):
                dist = np.norm(X[i] - X[j])
                res += float("Inf") if dist == 0 else 1 / dist
        return res
