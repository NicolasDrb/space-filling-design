from algo import NSGAII
import constants as const


def main():
    MU = const.MU
    LAMBDA = const.LAMBDA
    CXPB = const.CXPB
    MUTPB = const.MUTPB

    NGEN = const.NGEN
    n = const.NB_SAMPLES
    d = const.NB_VARIABLES

    algo_gen = NSGAII(MU, LAMBDA, CXPB, MUTPB, NGEN, n, d)
    algo_gen.run()
    algo_gen.plot()



if __name__ == "__main__":
    main()
