import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from seaborn import color_palette
from utils import normalize


def pretty_print_hof(hof, n, d):
    for i in range(len(hof)):
        ind = hof[i].reshape(n, d)
        print("======================================================")
        print("Individual #" + str(i))
        print(ind)
        visualize(ind)


def generate_label(pareto):
    return ["Ind" + str(i) for i in range(len(pareto))]


def visualize_repartition(ind, num, kl):
    nb_samples, nb_dim = ind.shape

    res = np.empty(shape=nb_dim)

    for axe in range(nb_dim):
        x = ind[:, axe]
        res[axe] = kl.H(x, 1)

    print(res)
    res = np.absolute(res)

    plt.figure(num="Repartition - Individual " + str(num))
    plt.xticks(range(nb_dim))
    plt.bar(range(nb_dim), res, align="edge",
            width=0.9, color=color_palette("Blues_d", n_colors=nb_dim))
    plt.show(block=False)


def visualize_axes(ind, num):
    nb_samples, nb_dim = ind.shape
    plt.figure(num="Axes individual " + str(num))

    numrows = nb_dim / 2
    numcols = nb_dim / 2

    if nb_dim > 5:
        numcols = (nb_dim - 1) / 2

    if nb_dim > 8:
        numcols = (nb_dim - 3) / 2
        numrows = (nb_dim - 1) / 2

    for d in range(nb_dim):
        plt.subplot(numrows, numcols, d + 1)
        plt.plot(ind[:, d], np.zeros_like(ind[:, d]) + 0, 'x')
        plt.title('Dimension ' + str(d + 1))

    plt.tight_layout()
    plt.show(block=False)


def visualize(ind, num):
    ind = ind.transpose()
    nb_plots = ind.shape[0]

    numrows = nb_plots / 2
    numcols = nb_plots / 2

    if nb_plots > 5:
        numcols = (nb_plots - 1) / 2

    if nb_plots > 8:
        numcols = (nb_plots - 3) / 2
        numrows = (nb_plots - 1) / 2

    plt.figure("Dimensions individual " + str(num))

    for i in range(0, nb_plots - 1):
        plt.subplot(numrows, numcols, i + 1)
        plt.title("Dim " + str(i) + ":" + str(i+1))
        plt.xlabel("dimension " + str(i))
        plt.ylabel("dimension " + str(i+1))
        plt.scatter(ind[i], ind[i+1])

    plt.tight_layout()
    plt.show(block=False)


def visualize_ind(ind, title, kl):
    print("======================================================")
    print("Individual #" + str(title))
    visualize_repartition(ind, title, kl)
    visualize_axes(ind, title)
    visualize(ind, title)
    plt.show(block=False)


def visualize_individuals(hof, n, d):
    for i in range(len(hof)):
        ind = hof[i].reshape(n, d)
        visualize_ind(ind, i)


def visualize_pareto(hof, indexes, lhd_score=None, titles=None):
    hof_fitness = np.asarray([[ind.fitness.values[indexes[0]], ind.fitness.values[indexes[1]]] for ind in hof])
    labels = generate_label(hof_fitness)

    if lhd_score:
        lhd_score = [lhd_score[indexes[0]], lhd_score[indexes[1]]]
        labels.append("LHD")
        hof_fitness = np.vstack([hof_fitness, lhd_score])

    norm_hof_fitness = normalize(hof_fitness)

    plt.figure()
    plt.title("Normalized Pareto front ")
    if titles is not None:
        plt.xlabel(titles[indexes[0]])
        plt.ylabel(titles[indexes[1]])
    else:
        raise Exception("No title provided")
    for label, x, y in zip(labels, norm_hof_fitness[:, 0], norm_hof_fitness[:, 1]):
        plt.text(x, y+0.001, label, fontsize=9)
        plt.scatter(x, y)
    plt.show(block=False)

def visualize_criterion(name, ngen, values):
    plt.figure()
    plt.title(name + ' criterion')
    plt.plot(range(0, ngen+1), values)
    plt.xlabel('Generation')
    plt.ylabel(name)
    plt.show(block=False)


def visualize_chosen_individuals(hof, n, d, kl):
    a = ""
    a = input("Enter the individual you want to display (input 'exit' otherwise)\n")
    while a != "exit":
        try:
            num = int(a)
            ind = hof[num].reshape(n, d)
            print("Plot of Individual #" + str(num))
            visualize_repartition(ind, num, kl)
            visualize_axes(ind, num)
            visualize(ind, num)
            a = input(
                "Enter the individual you want to display (input 'exit' otherwise)\n")
        except ValueError and IndexError:
            print("Value not in " + str(len(hof)))
            a = input(
                "Enter the individual you want to display (input 'exit' otherwise)\n")
    return
