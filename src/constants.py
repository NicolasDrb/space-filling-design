# Hyperparameters MO
NB_VARIABLES = 10
NB_SAMPLES = 30
NGEN = 3

# Hyperparameters NSGAII
MU = 10
LAMBDA = 50
CXPB = 0.5
MUTPB = 0.5
