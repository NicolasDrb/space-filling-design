# Construction de space-filling designs par optimisation multi-objectifs

## Critères de "remplissage"

- Kullback–Leibler en dimension d
- somme des critères sur chaque axe

## Algorithme d’optimisation multi-objectifs

NSGA-2

## Algorithme d’optimisation mono-objectif

## Critère de répartition uniforme de l’entropie entre les axes

## Répartition gaussienne des points sur les axes

## Références
